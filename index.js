document.getElementById("btn-ex1").addEventListener("click", () => {
  var benchmark = document.getElementById("txt-benchmark")?.value * 1;
  var point1 = document.getElementById("txt-point1")?.value * 1;
  var point2 = document.getElementById("txt-point2")?.value * 1;
  var point3 = document.getElementById("txt-point3")?.value * 1;

  var zone = document.getElementsByName("zone")[0]?.value * 1;
  var subject = document.getElementsByName("subject")[0]?.value * 1;
  var priority = zone + subject;
  var total = 0;

  var result = null;
  if (point1 > 0 && point2 > 0 && point3 > 0) {
    total = point1 + point2 + point3 + priority;
    result = total >= benchmark ? `Bạn đã đậu` : `Bạn đã thi trượt`;
  } else {
    result = `Bạn đã thi trượt`;
  }
  document.getElementById(
    "result1"
  ).innerHTML = `${result}. Tổng điểm ${total}`;
});

document.getElementById("btn-ex2").addEventListener("click", () => {
  var kW = document.getElementById("txt-kW")?.value;
  var name = document.getElementById("txt-name")?.value;
  var bill = null;

  if (kW <= 50) {
    bill = kW * 500;
  } else if (kW <= 100) {
    bill = 50 * 500 + (kW - 50) * 650;
  } else if (kW <= 200) {
    bill = 50 * 500 + 50 * 650 + (kW - 100) * 850;
  } else if (kW <= 350) {
    bill = 50 * 500 + 50 * 650 + 100 * 850 + (kW - 200) * 1100;
  } else {
    bill = 50 * 500 + 50 * 650 + 100 * 850 + 150 * 1100 + (kW - 350) * 1300;
  }
  console.log({ name, bill });
  var result = `Người dùng ${name} phải trả số tiền là ${bill}đ`;
  document.getElementById("result2").innerHTML = result;
});
